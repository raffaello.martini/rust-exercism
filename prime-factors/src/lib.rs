pub fn factors(n: u64) -> Vec<u64> {
    // unimplemented!("This should calculate the prime factors of {}", n)
    match n {
         0 | 1 =>  Vec::new(),
        _ => {
                for i in 2..=n {
                    if n % i == 0 {
                        return [vec![i], factors(n / i)].concat();
                        }
                    }
                 vec![n]
            },
    }
}


/*
Learning points:
  * the example was leaning toward recursion
  * match is super powerful
  * funny enough an empty list is different from an empty vector
  * to concatenate vectors: [vec![i], vec![j]].concat();

 */