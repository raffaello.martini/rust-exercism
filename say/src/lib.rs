macro_rules! zero {
    // zero!() -> String::from("Zero")
    () => { String::from("zero") }
}

const MAGNITUDES: [&str; 7] = ["", " thousand", " million", " billion", " trillion", " quadrillion", " quintillion"];

fn first_20(n: u64) -> Option<String> {
    let result = match n {
        0 => None,
        1 => Some("one"),
        2 => Some("two"),
        3 => Some("three"),
        4 => Some("four"),
        5 => Some("five"),
        6 => Some("six"),
        7 => Some("seven"),
        8 => Some("eight"),
        9 => Some("nine"),
        10 => Some("ten"),
        11 => Some("eleven"),
        12 => Some("twelve"),
        13 => Some("thirteen"),
        14 => Some("fourteen"),
        15 => Some("fifteen"),
        16 => Some("sixteen"),
        17 => Some("seventeen"),
        18 => Some("eighteen"),
        19 => Some("nineteen"),
        _ => panic!("Unexpected input"),
    }?.to_owned();
    Some(result)
}

fn tens_names(n: u64) -> String {
    match n {
        1 => "ten",
        2 => "twenty",
        3 => "thirty",
        4 => "forty",
        5 => "fifty",
        6 => "sixty",
        7 => "seventy",
        8 => "eighty",
        9 => "ninety",
        _ => panic!("Unexpected input")
    }.to_owned()
}

fn first_100(n: u64) -> String {
    if n > 100 {
        panic!("first_100 only works for n in range 0 .. 99")
    }
    if n < 20 {
        if let Some(d) = first_20(n) {
            d
        } else {
            zero!()
        }
    } else {
        let unit_digit = if let Some(u) = first_20(n % 10) {
            format!("-{}", u)
        } else {
            String::default()
        };
        let teen_digit = tens_names((n / 10) % 10);
        format!("{}{}", teen_digit, unit_digit)
    }
}

fn first_1000(n: u64) -> String {
    // 0 to 999
    if n >= 1000 {
        panic!("first_1000 works for n in range 0 .. 999");
    }
    let mut result: String;

    // first 100
    result = first_100(n % 100);

    // 100 to 999
    if let Some(c) = first_20((n / 100) % 10) {
        result = format_helper(&result, &c, " hundred");
    }
    result
}

pub fn encode(n: u64) -> String {
    let len = n.to_string().len();
    let mut result= zero!();
    // if len > 12 {
    //     panic!("Number too large.")
    // }
    for i in 0..=(len-1)/3 {
        let n = n / 1000u64.pow(i as u32) % 1000;
        let prefix = first_1000(n);
        result = format_helper(&result, &prefix, MAGNITUDES[i]);
    }
    result
}

// fn format_helper(result: &str, prefix: &str, magnitude: &str) -> String {
//     if result == zero!() {
//         format!("{}{}", prefix, magnitude)
//     } else if prefix == zero!() {
//         format!("{}", result)
//     } else {
//         format!("{}{} {}", prefix, magnitude, result)
//     }
// }

fn format_helper(result: &str, prefix: &str, magnitude: &str) -> String {
    if result == zero!() && prefix == zero!() {
        zero!()
        }
    else if result == zero!(){
        format!("{}{}", prefix, magnitude)
    } else if prefix == zero!() {
        format!("{}", result)
    } else {
        format!("{}{} {}", prefix, magnitude, result)
    }
}
