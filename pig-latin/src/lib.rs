use lazy_static::lazy_static;
use regex::Regex;

pub fn translate(input: &str) -> String {
    input.split(" ")
        .map(translate_word)
        .collect::<Vec<String>>()
        .join(" ")
}

fn translate_word(input: &str) -> String {
    lazy_static! {
        static ref VOWEL: Regex = Regex::new(r"^((?:h?[aeiou]+)|(?:yt)|(?:xr)).*").unwrap();
        static ref CONSONANT: Regex = Regex::new(r"^([a-z&&[^aeiou]]*)(.*)").unwrap();
        static ref QU: Regex = Regex::new(r"^([a-z&&[^aeiouq]]?(?:qu))(.*)").unwrap();
        static ref Y: Regex = Regex::new(r"^([a-z&&[^aeiouy]]+)(y.*)").unwrap();
        static ref Y2: Regex = Regex::new(r"^([a-z&&[^aeiouy]])y").unwrap();
    }
    if VOWEL.is_match(input) {
        return format!("{}ay", input);
    }
    if let Some(caps) = Y.captures(input) {
        return format!("{}{}ay", &caps.get(2).unwrap().as_str(), &caps.get(1).unwrap().as_str());
    }
    if let Some(caps) = Y2.captures(input) {
        return format!("y{}ay", &caps.get(1).unwrap().as_str());
    }
    if let Some(caps) = QU.captures(input) {
        return format!("{}{}ay", &caps.get(2).unwrap().as_str(), &caps.get(1).unwrap().as_str());
    }
    if let Some(caps) = CONSONANT.captures(input) {
        return format!("{}{}ay", &caps.get(2).unwrap().as_str(), &caps.get(1).unwrap().as_str());
    }

    panic!("Unexpected branch!")
}


#[cfg(test)]
mod tests {
    // use super::*;
    use regex::Regex;

    #[test]
    fn vowels() {
        let re = Regex::new(r"^((?:h?[aeiou]+)|(?:yt)|(?:xr))(.*)").unwrap();
        assert!(!re.is_match("rhythm"));
    }

    #[test]
    fn consonants() {
        let re = Regex::new(r"^([a-z&&[^aeiouq]]*(?:qu)?)(.*)").unwrap();
        let caps = re.captures("weewee");
        assert_eq!(caps.unwrap().get(1).unwrap().as_str(), "w");
        let caps = re.captures("pseudonimo");
        assert_eq!(caps.unwrap().get(1).unwrap().as_str(), "ps");
        let caps = re.captures("square");
        println!("{:?}", caps);
        assert_eq!(caps.unwrap().get(2).unwrap().as_str(), "are");
    }

    #[test]
    fn qu() {
        let re = Regex::new(r"^([a-z&&[^aeiouq]]?(?:qu)+)+(.*)").unwrap();
        let caps = re.captures("qat");
        println!("{:?}", caps);
        // assert_eq!(caps.unwrap().get(1).unwrap().as_str(), "w");
    }
}
