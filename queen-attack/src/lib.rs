#[derive(Debug)]
pub struct ChessPosition {
    rank: i32,
    file: i32,
}

#[derive(Debug)]
pub struct Queen{
    position: ChessPosition,
}

impl ChessPosition {
    pub fn new(rank: i32, file: i32) -> Option<Self> {
        if rank >= 0 && rank <8 && file >= 0 && file < 8{
            Some(Self {
                rank,
                file,
            })
        } else {
            None
        }
    }
}

impl Queen {
    pub fn new(position: ChessPosition) -> Self {
        Self{
            position
        }
    }

    pub fn can_attack(&self, other: &Queen) -> bool {
        // same rank
        if self.position.rank == other.position.rank {
            true
        // same file
        } else if self.position.file == other.position.file {
            true
        } else {
            self.can_attack_diagonal(other)
        }
    }

    fn can_attack_diagonal(&self, other: &Queen) -> bool {
        let d_rank = (self.position.rank - other.position.rank).abs();
        let d_file = (self.position.file - other.position.file).abs();
        // the two pieces are on a diagonal if the distance between rank and files is the same
        d_rank==d_file
    }
}
