# Rust Crash Course
## [Lesson 2 - Ownership](https://www.snoyman.com/blog/2018/10/rust-crash-course-02-basics-of-ownership)
Borrowing, references and mutable references

Copy and Clone Trait:
  * both can be derived
  * Copy is cheap, they compiler uses it instead of borrowing 
  * Clone is more expensive, has to be sexplicitely called out

Display trait:
  * Display trait, when implementing fmt, `write!` returns a Result
  
Closures:
  * use mutable closures if a variable is modified
  * if you need to access the variable modified by the closure (captured), pass it as argument

## [Lesson 3 - Iterators and Errors]()

### Command Lines arguments
`std::env::args` - method that returns arguments passed at startup
`std::env::Args` - iterator over args
```
let args = args();
for arg in args {
  println!("{}", arg);
}
```

`std::iter::skip` - method that skips some element in a Iterator
`std::iter::Skip` - Iterator that skips over n elements of an Iterator
```
let a = [1, 2, 3];

let mut iter = a.iter().skip(2);

assert_eq!(iter.next(), Some(&3));
assert_eq!(iter.next(), None);
```

`str::parse` - parse a string to get numbers etc
```
let four = "4".parse::<u32>();
assert_eq!(Ok(4), four);
```

## [Crates and more iterators]()

Using a crate:
  * copy the Cergo.toml section in the project `Cargo.toml` under `[dependencies]`

Reading a library it is an art.

For a game like, put the infinite loop in `main` - otherwise terminating the game is a pain :)

`Option<T>` now supports the `?` operator:
```
fn returns_option() -> Option<u32>:
    let x = another_fn_returns_option()?;
    return : Some(f(x))
```

Trait Implementation:
```
impl<I> Iterator for Doubler<I>
  where
  I: Iterator<u32>

```

`Doubler<I>`: This specifies what is the parameter for Doubler

`impl<I>`: This specify the variable we are implementing the Iterator trait for (I in this case).

`where I:Iterator<u32>`: This specify dependencies on the variable, in this case I has to implement Iterator for u32.

`where: I::Item: std::ops:::Mul<Outup=I::Item>` `I::item` multiplied will return `I::Item`

Chaining Iterators:
  * `for i in (1..11).skip(3).map(|x| x + 1).filter(|x| x % 2 == 0)` 
    * takes the range from 1 to 10 (11 is excluded)
    * skips the first 3 elements
    * adds 1
    * takes only the even results

Collect result:
`let my_vec: Vec<u32> = (1..11).collect(); // vec[1,2,3,4,5,6,7,8,9,10]`

`fold()` is the equivalent of reduce

`std::ops::{Add,Mul...}` generic functions

## [Parameters, Iterators, and Closures]()

### Parameters
3 ways to pass a parameter:
  * by value : move (or copy / clone)
    * mutable: the parameter can be changed in the function
    * immutable: the parameter can't be changed in the function
    * as any other veriable defined in the function (`mut` or not)
  * by immutable reference reference
  * by mutable reference
    * a function that requires an `& ref` will work with a `&mut ref` as it is more restrictive (not the other way around)

`fn (mut ref: &(mut)T ) -> T {}` Just like a moved paramenter, the reference is mutable (or not) only in the function.
* `&mut` -> `&`

### Iterators
This rules applies to iterators (and for loops) as well!!!
```
let mut nums = vec![1,2,3,4,5];
```
  * move: `for j in values` => value is moved
    * equivalent to `for j in numbs.into_iter()`
  * ref: `for j in &values` => value is passed by reference, j type is `&u32` 
    * equivalent to `for j in numbs.iter()`
  * mut ref: `for j in &mut values` => value is passed by reference, j type is `&mut u32`
    * equivalent to `for j in numbs.iter_mut()`

`into_iter()` is part of the `IntoIterator` trait.
In a for loop, `into_iter()` is used.

## Closures
Similar to *functions* that call on some *arguments*, 
but unlike them *closures* capture values from *local scope*.

It is possible to define a function in a function.
Use *closures* instead of functions when you need to access (capture) variables without passing them as arguments.

### Immutable Closure - Fn
Closures *types* are **anonymous**, they are actually implementing the *trait* **Fn**
To pass them as parameter:
```
fn takes_a_closure<F>(f: F)
  where F: Fn(Tin) -> Tout {
    ...
  }
```

### Mutable Cloruse - FnMut
When a closure is capturing a variable and changing it, it has to be defined as **mut**.
A mut closure implements the **FnMut** *Trait*
When passed as *parameter* requires to be `mut`
```
fn requires_mut_closure<F>(mut f: F)
    where F: FnMut(), { }
```
 A closure that has the `Fn` trait, is also `FnMut` - similar to a parameter that is `&mut` is also `&`

### Move closure - FnOnce
A closure that is moving a variable in.
```
fn call_once<F>(f: F)
where F: FnOnce(), {
  f();
}
```

All `Fn` are `FnMut`; `FnMut` are also `FnOnce`:
* `Fn` -> `FnMut` -> `FnOnce`

### Move in Closures
Cosures can own data, functions don't.
The `move` keyword moves in the closure all captured data.
It is used to solve lifetime issues by instructing the compiler to move the data in the closure.

## Reference counting

### Rc
```
let file = std::fs::File::create("mylog.txt").unwrap();
let file = Rc::new(file);
```
More about `Rc`:
  * used to provide shared ownership
    * `.clone()` to create a new reference
  * useful for closures that have to implement the Fn trait
  * `Rc` implements `Deref`:
    * Rc<T> -> all methods that applie to T can be applied to Rc
  * `Rc` is immutable

### RefCell
Used to have multiple mutable references.
`RefCell`:
  * does not implement Deref
  * need to use a method to get the refenrece
    * borrow()
    * borrow_mut()

`Rc` and `RefCell` go together: 
  * `Rc` solves the reference counting
  * `RefCell` solves mutability

### Concurrency
`std::thread::spawn`: spawn a thread.
```
pub fn spawn<F, T>(f: F) -> JoinHandle<T> where
  F: FnOnce() -> T,
  F: Send + 'static,
  T: Send + 'static,
```
Some details:
  * `Send`: both the function and the return value can be sent to a differet Thread
    * no Rc, Cell, RefCell
  * `'static`: can't keep references to local variables
  * `FnOnce()`: every closure can work

`Arc` - use it instead of `Rc` in multi thread  
`Mutex` - corresponds to `RefCell` in multi thread 

## Array, Vectors and slices

### Array `[T, size]`
Contiguous sequence of element with fixed size and direct access.

### Vector `Vec<T>`
Contiguous sequence of element with dynamic size and direct access.

### Slice `&[T]`
Contiguous sequence of element in a collection - not the full collection.

* It is the minimum common denominator between Array and Vectors
* given `vector: Vector<T>`,  `&vector` can be deref to either a slice or to a vector ref depending on the function signature.

## Byte literals `b"String"`
  * Allocated on the stack.
  * They can only be accessed as reference.
  
