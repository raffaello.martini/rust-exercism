use num::integer::gcd;

#[derive(PartialEq, Eq, Debug)]
pub enum Bucket {
    One,
    Two,
}

impl  Bucket {
    fn other(&self) -> Bucket {
        match self {
            Bucket::Two => Bucket::One,
            Bucket::One => Bucket::Two,
        }
    }
}

/// A struct to hold your results in.
#[derive(PartialEq, Eq, Debug)]
pub struct BucketStats {
    /// The total number of "moves" it should take to reach the desired number of liters, including
    /// the first fill.
    pub moves: u8,
    /// Which bucket should end up with the desired number of liters? (Either "one" or "two")
    pub goal_bucket: Bucket,
    /// How many liters are left in the other bucket?
    pub other_bucket: u8,
}

#[derive(Copy, Clone, Debug, PartialEq)]
struct State(u8, u8);

#[derive(Copy, Clone, Debug)]
struct Size(u8, u8);

#[derive(Copy, Clone, Debug)]
struct SolveHelper {
    state: State,
    size: Size,
}

impl SolveHelper {
    fn with_input(cap1: u8, cap2: u8) -> Self {
        Self {
            state: State(cap1, 0),
            size: Size(cap1, cap2),
        }
    }

    fn is_complete(&self, goal: u8) -> Option<(Bucket, u8)> {
        let State(x, y) = self.state;
        if x == goal {
            Some((Bucket::One, y))
        } else if y == goal {
            Some((Bucket::Two, x))
        } else {
            None
        }
    }

    fn next(&self) -> Self {
        let Size(max_x, max_y) = self.size;
        let State(x, y) = self.state;
        let state =
            if x == max_x || y == 0 {
                if x + y >= max_y {
                    //     will hit the top
                    State(x + y - max_y, max_y)
                } else {
                    //     will hit the left
                    State(0, x + y)
                }
            } else if y == max_y {
                State(x, 0)
            } else if x == 0 {
                State(max_x, y)
            } else {
                panic!("crash and burn!")
            };
        return Self {
            state,
            size: self.size,
        };
    }
}

/// Solve the bucket problem
pub fn solve(
    capacity_1: u8,
    capacity_2: u8,
    goal: u8,
    start_bucket: &Bucket,
) -> Option<BucketStats> {
    if goal % gcd(capacity_1, capacity_2) != 0 {
        return None;
    }
    // special case: needs a different starting point
    if capacity_2 == goal && start_bucket==&Bucket::One ||
        capacity_1 == goal && start_bucket==&Bucket::Two  {
        return Some(BucketStats{
            moves : 2,
            goal_bucket: start_bucket.other(),
            other_bucket: capacity_1,
        })
    }
    let (mut state, swap) = match start_bucket {
        Bucket::One => (
            SolveHelper::with_input(capacity_1, capacity_2),
            false,
        ),
        Bucket::Two => (
            SolveHelper::with_input(capacity_2, capacity_1),
            true,
        ),
    };
    let mut moves = 1u8;
    loop {
        if let Some((goal_bucket, other_bucket)) = state.is_complete(goal) {
            return Some(BucketStats {
                moves,
                goal_bucket: if swap { goal_bucket.other() } else { goal_bucket },
                other_bucket,
            })
        }
        state = state.next();
        moves += 1;
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_solver_helper() {
        let mut state = SolveHelper::with_input(3,5);
        assert_eq!(state.state, State(3,0));
        state = state.next();
        assert_eq!(state.state, State(0,3));
        state = state.next();
        assert_eq!(state.state, State(3,3));
        state = state.next();
        assert_eq!(state.state, State(1,5));
        state = state.next();
        assert_eq!(state.state, State(1,0));
    }
}