use std::collections::HashMap;
use std::fmt;
use std::cmp::Ordering;


enum Result {
    Win,
    Loss,
    Draw,
}

impl Result {
    fn score(&self) -> usize {
        match self {
            Self::Win => 3,
            Self::Draw => 1,
            Self::Loss => 0,
        }
    }

    fn opposite(&self) -> Self {
        match self {
            Self::Win => Self::Loss,
            Self::Draw => Self::Draw,
            Self::Loss => Self::Win,
        }
    }
}

#[derive(Hash, Eq, PartialEq, Ord, PartialOrd, Clone)]
struct TeamResults {
    p: usize,
    name: String,
    mp: usize,
    w: usize,
    d: usize,
    l: usize,
}

impl TeamResults {
    fn new(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            mp: 0,
            w: 0,
            d: 0,
            l: 0,
            p: 0,
        }
    }

    fn update(&mut self, result: Result) {
        self.mp += 1;
        match result {
            Result::Win => self.w += 1,
            Result::Draw => self.d += 1,
            Result::Loss => self.l += 1
        }
        self.p += result.score();
    }
}

impl fmt::Display for TeamResults {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:<31}| {:>2} | {:>2} | {:>2} | {:>2} | {:>2}",
               self.name, self.mp, self.w, self.d, self.l, self.p)
    }
}

struct TeamResultDb (HashMap<String, TeamResults>);

impl TeamResultDb {
    fn new() -> Self {
        Self(HashMap::new())
    }

    fn update(&mut self, team: &str, result: Result) {
        self.0.entry(team.to_owned())
            .or_insert(TeamResults::new(team))
            .update(result);
    }

    /// Return a Vec containing all score lines, in order
    fn to_lines(&self) -> Vec<String> {
        let mut vec = self.0
            .values()
            // .map(|e| e.to_owned())
            .collect::<Vec<_>>();
        // vec.sort();
        // vec.reverse();
        vec.sort_by(|a, b| {
            match a.p.cmp(&b.p).reverse() {
                Ordering::Equal => a.name.cmp(&b.name),
                other => other,
            }
        });
        vec.iter()
            .map(|e| format!("{}", e))
            .collect()
    }
}


pub fn tally(match_results: &str) -> String {
    // initialize the result table
    let mut team_results = TeamResultDb::new();

    for match_line in match_results.lines() {
        // parse the match result
        let pattern: Vec<_> = match_line.split(';').collect();
        let home_team = pattern[0].to_string();
        let visitor_team = pattern[1].to_string();
        let home_result = match pattern[2] {
            "win" => Result::Win,
            "loss" => Result::Loss,
            "draw" => Result::Draw,
            _ => panic!("Error while parsing line\n\
                        {}\n\
                        Unexpected result {}.", match_line, pattern[2]),
        };
        let visitor_result = home_result.opposite();

        // updates the scores
        team_results.update(&home_team, home_result);
        team_results.update(&visitor_team, visitor_result);
    }
    let report = team_results.to_lines();
    let header = "Team                           | MP |  W |  D |  L |  P";
    let mut result = format!("{}", header);
    for line in report {
        result = format!("{}\n{}",result,line);
    }
    result
}
