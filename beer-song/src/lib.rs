pub fn verse(n: u32) -> String {
    // unimplemented!("emit verse {}", n)

    let (bottles, action, next_bottles) = match n {
        0 => (bottles(n),
              "Go to the store and buy some more",
              bottles(99)  ),
        1 => (bottles(n),
              "Take it down and pass it around",
              bottles(0)  ),

        _ => (bottles(n),
              "Take one down and pass it around",
              bottles(n-1) ),
    };
    format!("{} of beer on the wall, {} of beer.\n{}, {} of beer on the wall.\n",
            uppercase_first_letter(&bottles), bottles, action, next_bottles)
}

fn bottles(n: u32) -> String {
    match n {
        0 => "no more bottles".to_string(),
        1 => "1 bottle".to_string(),
        _ => format!("{} bottles",n),
    }
}



fn uppercase_first_letter(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
}

pub fn sing(start: u32, end: u32) -> String {
    let mut song = "".to_string();
    for i in (end+1..=start).rev(){
        song.push_str(&verse(i));
        song.push_str("\n");
    }
    song.push_str(&verse(end));
    song
}

/*
pub fn sing(start: u32, end: u32) -> String {
    (end..=start)
        .rev()
        .map(verse)
        .collect::<Vec<String>>()
        .join("\n")
}
*/