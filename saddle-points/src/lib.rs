fn max(row: &[u64]) -> (Option<u64>, Vec<usize>) {
    let mut result = Vec::new();
    let mut max_o: Option<u64> = None;
    for (i, &e) in row.iter().enumerate() {
        if let Some(max) = max_o {
            // past first element elements
            if e > max {
                max_o = Some(e);
                result = vec![i];
            } else if e == max {
                result.push(i)
            }
        } else {
            // First element
            max_o = Some(e);
            result = vec![i];
        }
    }
    (max_o, result)
}

fn is_min(input: &[Vec<u64>], col: usize, value: u64) -> bool {
    let min = input.iter().map(|e| e[col] ).min();
    if let Some(min) = min {
        if min == value {
            return true
        }
    }
    false
}


pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    // let rows = input.len();
    // let cols =  input[0].len();
    let mut result = Vec::new();
    for (i, row) in input.iter().enumerate() {
        let (max, indexes) = max(row);
        //     non empty
        if let Some(value) = max {
            for j in indexes {
                if is_min(input, j, value) {
                    result.push((i, j))
                }
            }
        }
    }
    result
}
