use std::collections::BinaryHeap;
use std::cmp::Reverse;

#[derive(Debug)]
pub struct HighScores<'a> {
  scores: &'a [u32],
}

impl<'a> HighScores<'a> {
    pub fn new(scores: &'a[u32]) -> Self {
      HighScores {
        scores: scores,
      }
    }

    pub fn scores(&self) -> &'a[u32] {
        self.scores
    }

    pub fn latest(&self) -> Option<u32> {
        self.scores.last().cloned()

    }

    pub fn personal_best(&self) -> Option<u32> {
        self.scores.iter().max().cloned()
    }

    pub fn personal_top_three(&self) -> Vec<u32> {
        let len: usize = 3;
        let scores = self.scores;
        let mut top3 = BinaryHeap::new();
        for &i in scores {
            if top3.len() < len { top3.push(Reverse(i)); }
            else {
                let &Reverse(nth) = top3.peek().unwrap();
                if i > nth {
                    top3.push(Reverse(i));
                    top3.pop(); // make sure there are only 3 values in the heap
                }
            }
        }
        top3.into_sorted_vec().into_iter().map(|Reverse(x)| x ).collect()
    }
}

/*
Learning points:

  * Lifetimes guide the compiler to track when a reference is valid and keep the value in memory
  * Binary heap is pretty cool to get a priority queue (default to max)
  * if you want a min heap the API is a PAIN.

*/