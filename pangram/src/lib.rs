use std::collections::HashSet;

struct CounterHelper(HashSet<char>);

impl CounterHelper {
    fn new() -> Self {
        Self(HashSet::new())
    }

    fn parse(&mut self, c: char) {
        if c.is_ascii_alphabetic() {
            self.0.insert(c.to_ascii_lowercase());
        }
    }

    fn is_complete(& self) -> bool {
        let mut complete: HashSet<char> = HashSet::new();
        for c in (b'a'..=b'z').map(char::from) {
            complete.insert(c);
        }
        if self.0 == complete { true } else { false }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_complete(){
        let mut counter = CounterHelper::new();
        for c in (b'a'..=b'z').map(char::from) {
            counter.parse(c);
        }
        assert!(counter.is_complete());
    }

    fn test_incomplete(){
        let mut counter = CounterHelper::new();
        for c in "Raffo!".chars() {
            counter.parse(c);
        }
        assert!(!counter.is_complete());
    }


}

/// Determine whether a sentence is a pangram.
pub fn is_pangram(sentence: &str) -> bool {
    let mut helper = CounterHelper::new();
    for c in sentence.chars() {
        helper.parse(c);
    }
    helper.is_complete()
}
