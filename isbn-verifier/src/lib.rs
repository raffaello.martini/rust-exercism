use regex::Regex;

fn parse(isbn: &str) -> Option<Vec<u32>>{
    let mut result: Vec<u32> = Vec::new();
    let re = Regex::new(r"^(\d)-?(\d)(\d)(\d)-?(\d)(\d)(\d)(\d)(\d)-?([\dX])$").unwrap();
    let caps = re.captures(isbn)?;
    // it is a match
    for i in 1..=10 {
        // build the vector of results
        let d = caps.get(i).unwrap().as_str();
        let value: u32 = match d {
            "X" => 10,
            _ => d.parse().unwrap(),
        };
        result.push(value);
    }
    Some(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_valid_w_hyphens() {
        let text = "3-598-21508-8";
        let exp = Some(vec![3,5,9,8,2,1,5,0,8,8]);
        assert_eq!(exp,parse(text));
    }

    #[test]
    fn parse_valid_no_hyphens() {
        let text = "3598215088";
        let exp = Some(vec![3,5,9,8,2,1,5,0,8,8]);
        assert_eq!(exp,parse(text));
    }

    #[test]
    fn parse_invalid() {
        let text = "3-598-21508-89";
        // let exp = Some(vec![3,5,9,8,2,1,5,0,8,8]);
        assert_eq!(None,parse(text));
    }
}


/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {
    if let Some(num) = parse(isbn) {
        if (num[0] * 10 + num[1] * 9 + num[2] * 8 +
            num[3] * 7 + num[4] * 6 + num[5] * 5 +
            num[6] * 4 + num[7] * 3 + num[8] * 2 +
            num[9] ) % 11 == 0 {
            return true
        }
    }
    false
}
