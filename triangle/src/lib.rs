// extern crate num_traits;
use std::iter::Sum;
use core::ops::Mul;


pub struct Triangle<T>([T; 3]);

impl<T> Triangle<T>
    where T: Sum + Mul<Output=T> + PartialEq +
    From<u8> + Copy + PartialOrd {
    pub fn build(sides: [T; 3]) -> Option<Triangle<T>> {
        let perimeter: T = sides.iter().map(|x| *x).sum();
        for side in &sides {
            // x < y + z
            // x + y + z = 2p
            // 2x < 2p
            let zero: T = 0u8.into();
            let two: T = 2u8.into();
            if (*side == zero) || !((two * *side) < perimeter) {
                return None;
            }
        }
        Some(Triangle(sides))
    }

    pub fn is_equilateral(&self) -> bool {
        let [a, b, c] = self.0;
        a == b && a == c
    }

    pub fn is_scalene(&self) -> bool {
        let [a, b, c] = self.0;
        a != b && a != c && b != c
    }

    pub fn is_isosceles(&self) -> bool {
        let [a, b, c] = self.0;
        a == b || a == c || b == c
    }
}
