#[derive(Debug, PartialEq, Eq)]
pub enum Classification {
    Abundant,
    Perfect,
    Deficient,
}

fn sum_divisors(n: u64) -> u64 {
    (1..=n/2).filter(|i| n % i == 0).sum()
}

pub fn classify(num: u64) -> Option<Classification> {

    if num == 0 {
        return None
    }

    let sum_divisors: u64 = sum_divisors(num);

    if sum_divisors == num { Some(Classification::Perfect) }
    else if sum_divisors > num { Some(Classification::Abundant) }
    else { Some(Classification::Deficient) }
}
