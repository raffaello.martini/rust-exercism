use ascii::AsciiChar;

/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
    plain.chars()
        .filter(char::is_ascii_alphanumeric)
        .map(encode_letter)
        .collect::<Vec<char>>()
        .chunks(5)
        .map(|c| c.iter().collect::<String>())
        .collect::<Vec<String>>()
        .join(" ")
}

/// encodes a character, it MUST be ASCII
fn encode_letter(c: char) -> char {
    let ac = AsciiChar::from_ascii(c).unwrap().to_ascii_lowercase();
    if ac.is_alphabetic() {
        AsciiChar::from_ascii(b'z' - (ac.as_byte() - b'a')).unwrap().as_char()
    } else {
        ac.as_char()
    }
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    cipher.chars()
        .filter(char::is_ascii_alphanumeric)
        .map(encode_letter)
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1(){
        assert_eq!('a', encode_letter('z'));
        assert_eq!('d', encode_letter('w'));
    }
}
