/*
on every year that is evenly divisible by 4
  except every year that is evenly divisible by 100
    unless the year is also evenly divisible by 400
 */

pub fn is_leap_year(year: u64) -> bool {
    if year % 4 == 0 {
        match (year % 100 == 0, year % 400 == 0) {
            (true, true) => return true,
            (true, false) => return false,
            (false, _) => return true,
        }
    }
    false
}
