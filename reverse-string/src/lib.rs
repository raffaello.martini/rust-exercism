use unicode_segmentation::UnicodeSegmentation;

pub fn reverse(input: &str) -> String {
    // let bytearray : Vec<_> = input.as_bytes().iter().rev().copied().collect();
    // String::from_utf8_lossy(&bytearray).to_string()
    UnicodeSegmentation::graphemes(input, true)
        .rev()
        .collect()
}


/*
Learning points:
  * strings are a pain because UTF8 is a pain
  * the unicode_segmentation crate is AWESOME!
  * Itarator<Item=&str>.collect() => String
 */