use std::collections::HashMap;

struct ScoreTable(HashMap <char, u64>);

impl ScoreTable {
    fn value(&self, c: char) -> u64 {
        self.0.get(&c.to_ascii_uppercase()).unwrap_or(&0u64).to_owned()
    }

    fn new() -> ScoreTable {
        let mut score = ScoreTable(HashMap::new());
        score.init_helper("A, E, I, O, U, L, N, R, S, T", 1);
        score.init_helper("D, G", 2);
        score.init_helper("B, C, M, P", 3);
        score.init_helper("F, H, V, W, Y", 4);
        score.init_helper("K", 5);
        score.init_helper("J, X", 8);
        score.init_helper("Q, Z", 10);
        score
    }

    fn init_helper(&mut self, keys: &str, value: u64) {
        for c in keys.split(", ")
            // this "converts" a single character String to a char
            .map(|s| s.chars().next().unwrap()){
            self.0.insert(c, value);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_score_table() {
        let table = ScoreTable::new();
        assert_eq!(table.value('A'), 1);
        assert_eq!(table.value('N'), 1);
        assert_eq!(table.value('Q'), 10);
        assert_eq!(table.value('V'), 4);
    }
}



/// Compute the Scrabble score for a word.
pub fn score(word: &str) -> u64 {
    let score_table = ScoreTable::new();
    word.chars()
        .map(|c| score_table.value(c))
        .sum()
}


