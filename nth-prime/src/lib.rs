pub fn nth(n: u32) -> u32 {
    if n == 0 {
        return 2;
    }
    let mut primes = vec![2];
    for _ in 1..=n {
        primes.push(find_next(&primes));
    }
    primes[n as usize]
}

fn find_next(previous: &Vec<u32>) -> u32 {
    let mut candidate = *(previous.last().unwrap());
    loop {
        if previous.iter().all(|&x| candidate % x != 0) {
            return candidate;
        }
        candidate += 1;
    }
}
