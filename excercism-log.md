# [Simple Linked List](https://exercism.io/my/solutions/9f24482d38f24610a65a151e93ade768)
Use `Box<T>` whenever you want to create self referring structures, example:
```
struct Node<T>{
    next: Option<Node<T>>,
    data: T,
}
```
This won't compile as the it doesn't have a defined size.

`mem::replace` - gets a value out of a mut borrow and replace it with something else.
`Option` type has a method that replaces `mem::replace(&mut option, None)` : `option.take()`

A great resource for understanding: a guide to building linked list in Rust - [link](https://rust-unofficial.github.io/too-many-lists/third.html)

## Option functions

### `if let Some(mut x) = option {}`
The `mut` allows to modify x.

### `take()`
`new_ref = option.take(); // option = None`
  Takes (**move**) the value out of option and assigns None to it.

  Useful to manipulate &mut ref. 

### `as_ref()`
`ref = option.as_ref() // ref: Option<&T>`
  * gets the reference to the content of Option and wraps it in Option
  * Similar to how `take()` is used to manipulate a &mut ref, `as_ref()` is used to get value by reference

### `as_mut()`
`mut_ref = option.as_mut() // new_ref: Option<&mut T>`
  * gets a mutable reference to the value of the option and wraps it in a Option

### `map()`
Saves some work when accessing the value of the option and returning an option.
`match option { None => None, Some(x) => Some(f(x)) }` => `option.map()`
* the closure - f(x) must return the type to be wrapped in Option.

### `and_then()`
Similar to map, but the inner function is expected to return an Option.

## Other learnings
`fn method(self)` is taking ownership of self.
  * If you want to mutate anything, just change it into `fn method(mut self)`

`match x { Some(ref x) => ...}`
  * the `ref` keuword in matching forces to get the reference

## Iterator pattern:
### `IntoIter`
This is destructive, the value is moved in the iterator:

* define helper struct `IntoIter<T>` as wrapper around the original structure (List in this case)
* implement the method `into_iter(self) -> IntoIter<T>` for the structure
  * note: it returns the helper structure
* implement the `Iterator` **trait** for the helper structure

### `Iter`
This is non destructive: the iterator is a reference to the element in the list:

* define the helper structure `Iter<T>` as a wrapper
* implement `iter(&self) -> Iter<T>` for the structure
  *  Note: it returns the helper structure
* implements the `Iterator` trait for the helper structure

### `IterMut`
The iterator is a mut ref to the elements:

* define the helper structure `IterMut<T> as wrapper
* implement `iter_mut(&mut self) -> IterMut<T>` 
* implement the `Iterator` trait for IterMut

## Rc
Ref count, used to have multiple references to the same data.

### `clone()`
Create a new ref adding the ref count.

### `try_unwrap()`
Returns the innver value (**move**) if the reference has count 1.

## RefCell
Provides inner mutability - aka can be mutated by `&ref` - for all types (`Cell` is similar but requires `Copy` trait).
RefCell does not Deref automatically. To access values you must borrow.
### `borrow(&self)
`fn borrow(&self) -> Ref<'_, T>;`
Similar to ref, can have more than 1 copy at any given time.
It is implemented at runtime, if it fail, it crashes.

### `borrow_mut(&self)
`fn borrow_mut(&self) -> RefMut<'_, T>;`
Similar to mut ref, can only have 1.
It is implemented at runtime, if it fail, it crashes.

### `into_inner()`
Consumes the cell and returns the inner data.
Note: to make this work for Rc<RefCell> you have to take RefCell out of Rc before applying.

### `Ref::map()`
`pub fn map<U, F>(orig: Ref<'b, T>, f: F) -> Ref<'b, U>`
Can be used to map Ref<T> -> Ref<U>, pretty convenient.

## Double Ended Iterators
`DoubleEndedIterator` implements `Iterator` trait.
It adds a second method: `next_back()` that yields elements from the end.

# [DOT DSL](https://exercism.io/my/solutions/8219e8e33855437ab8bf0d75b0659d5c)

## Builder pattern


There 2 flavor of Builder pattern, I had to use the consuming pattern to allow one-liner configuration.

It is easier using `String` than `&str` as Hash map key
  * `Hashmap<String, _>::get(x)` allows x both as `&str` and `String`

## "Inheritance"

Rust doesn't have inheritance like a OOP language:

  * Trait to define common behaviour across Struct, they serve the purpose of interfaces in Java
  * To extend a struct, you can nest a base structure in other structures
  * [https://users.rust-lang.org/t/how-to-implement-inheritance-like-feature-for-rust/31159]
  * [Is rust an OOP language] [https://users.rust-lang.org/t/is-rust-oop-if-not-what-is-it-oriented-to/30777/6]
  * Rust Koans [https://users.rust-lang.org/t/rust-koans/2408]

## Modules
  * A crate can have multiple modules
  * modules can have submodules
  * every module or submodule can be public or private
  * an item (function, method, structure, Trait etc) has to be `pub` **from the root down** to be externally available
  * a module has access to pub items under a private submodule (example attributes)
  * there are visibility modifiers to make everything more granular!!!

# [Pascal's triangle](https://exercism.io/my/solutions/4fd7f14e041f40bdafa780d827dbe5ad)
Vec to slice and back.
```
let vec: Vec<u32> = vec![1,2,3];
let slice: &[u32] = &vec;
let slice = &vec[..]; // alternative
let back_to_vec = &vec[..].iter().collect();
```

Concatenate slices:
```
let slice1 = &[0,1]; let slice2 = &[1,2]; 
[slice1, slice2].concat() // &[0,1,2,3]
```

# [PaaS I/O](https://exercism.io/my/solutions/60a6bf6544ec4b58804daba1eeab8385)

## `std::marker::PhantomData`
```
pub struct ReadStats<R>(::std::marker::PhantomData<R>);
```
The PhantomData is used to help the compiler to deal with Types when there is no field with a type in the structure.
In this case is used to allow a generic type R that implements Read or Write.

# [Nucleotide Count](https://exercism.io/my/solutions/b48e6858b5004760a9470937ff12ffe7)

## `std::iter::try_fold`
Similar to fold, but deals with Return type.
```
dna.chars().try_fold(0, |count, elem| {
  match elem {
    'A' | 'C' | 'G' | 'T' => {
        if nucleotide == elem { Ok(count + 1) }
       else { Ok(count) }
     }
  _ => Err(elem)
  }
})
```
Pretty awesome!

## `std::iter::for_each`
Somehow similar to map, applies a closure to every element in an Iterator.
Can be used to replace a for loop in a functional way.

What's the difference between `map` and `for_each`?\
`map` is lazyly evaluated, `for_each` is not.
https://willmurphyscode.net/2018/05/30/for_each-map-and-compiler-warnings/

## `std::iter::try_for_each`
Variation of for_each that unwraps Results and stops at the first error.

## Hashmaps
Typical pattern for a frequency counter:
```
let mut count = HashMap::new();
for x in iter {
  *count.entry(elem).or_insert(0) += 1;
}
```

# [ETL](https://exercism.io/my/solutions/4dc7a2f5215142fb9b357e6d9f353ed4)
Some practice with `BTreeMap` - a Map implemented with a binary tree.
Useful when you need ordered keys - probably a HashMap was a better data structure here.

# [Acronym](https://exercism.io/my/solutions/b1f09bd363d544aa87101cffcc802172)
## `std::option::Option::map_or(default, f)`
Applies function `f` if there is a value or assigns a `default` value - pretty nice.

Parsing strings is a nightmare :)

`split()`, `match()` and similar accept a pattern.
I found using builtin `char` methods is quite nice:
```
.split(char::is_whitespace)
```
Also, the pattern could be a function `Fn(char) -> Bool`

# [Sieve](https://exercism.io/my/solutions/f9008bd85bec48cdac47f3bb1a4908c4)
I opted for a solution that didn't require index manipulation:
  * a `BTreeSet` to represent the Sieve
  * a `Vector` to push primes into as they compile

Main problem: paying attention to the upper bounds in ranges!!!

Some interesting ranges capabilities:
```
(current + 1..=self.upper_bound)
  .find(|x| !self.flagged.contains(x))
```
`find()` applies a closure to an iterator and return and option with the value that first matches the condition.

```
for m in (1..)
.take_while(|m| m * current <= upper_bound) {...}
```
Creates a counter and stops it with a condition - like having a for and a while combined!!

# RNA Transcription (https://exercism.io/my/solutions/df6a0c9a87f04472a682381c03568fc8)
Learning points:
  * public vs private
  * Return type and error propagation
  * `#[derive(PartialEq)]` so powerful: I can compare 2 DNA/RNA sequences without writing a line of code :D

## Error propagation with Iterators

`collect()`
  * can make a sequence of `Result<T,e>` into a `Result<Collection<T>,e>` !!

`?` Operator:
  * can also be used when assigning a variable, not just on methods !!
```rust
fn fn_returns_result() -> Result<T, e>{
  let r = other_fn_returns_result();
  Ok(fn_that_requires_type(r?))
}
```

# [Triangles](https://exercism.io/my/solutions/967bf31bd0424f46815b10967dea87db)
Aritmetic operations with generics aren't fun!
When you need a constant, it is possible to create the value from u8 with the `into()` method.
It will require the `From<u8>` trait.

```rust
impl<T> Triangle<T>
    where T: Sum + Mul<Output=T> + PartialEq +
    From<u8> + Copy + PartialOrd {
    pub fn build(sides: [T; 3]) -> Option<Triangle<T>> {
        let perimeter: T = sides.iter().map(|x| *x).sum();
        for side in &sides {
            let zero: T = 0u8.into();
            let two: T = 2u8.into();
  ...
```

# [Binary Search](https://exercism.io/my/solutions/232ed18449554c3596bc987933ed0d5c)
You want to generalized vectors, array and slices ? `AsRef` is your friend.

For example the initial signature was this:
```rust
pub fn find<T>(array: &[T], key: T) -> Option<usize>
    where T: Ord,
{
    ...
}
```

`AsRef` forces the array to have the `as_ref()` method, that will return our slice : wonderful!

```rust
pub fn find<A, T>(array: A, key: T) -> Option<usize>
    where
        A: AsRef<[T]>,
        T: Ord,
{
    let array = array.as_ref();
}
```

# [Robot Simulator](https://exercism.io/my/solutions/84c16731d6124817ba0a1e130c0c9d06)
No major learning point here.
The structure was defined in a way that all operations were immutable, it played well with `iter()` and `fold()`

# [Bowling](https://exercism.io/my/solutions/9db9dc19808a44caaf0d7002578378db)
Be EXTRA careful with how you define a state machine...
The complexity of the problem is how to carry over the state and all the possible decision trees.
This pattern to go through x element in a vector is kind of neat:
```rust
let v = [1,2,3,4,5,6,7,8,9]
for e in v.iter()
 .zip(v[1..10].iter()
     .chain(iter::repeat(&0i32)))
 .zip(v[2..10].iter()
     .chain(iter::repeat(&0i32)))
 .collect::<Vec<_>>() {
   println!("{:?}",e);
 }
// ((1, 2), 3)
// ((2, 3), 4)
// ...
// ((7, 8), 9)

```

# [Tournament](https://exercism.io/my/solutions/f490b48ff90e4e4d8c3385195078637b)
Main take away: **SORTING!!!!**
String parsing was ok - ish.
The main challenge of the excercise was sorting the score board by score descending with a tie breaker of tram name ascending.
```rust
struct Score {
  name: String,
  p: i32,
  // other fields not shown here
}

vec.sort_by(|a, b| {
    match a.p.cmp(&b.p).reverse() {
        Ordering::Equal => a.name.cmp(&b.name),
        other => other,
    }
```

# [Alphametics](https://exercism.io/my/solutions/7ba943fbb72b49d3bc38e841d102a757)
Lots of learning in this one.

## First challenge: parsing.
The challenge here was to remove all whitestpaces AND parse the separator\
_(I could have include the spaces in the separator but it felt kid of cheating)_

**TL;DR: Never trust the REPL** - I have literelly wasted 1 hour to fight the borrow checker when there was nothing to fight.
```rust
let input = input
            .chars()
            .filter(|c| !c.is_whitespace())
            .collect::<String>();
let parser = input
             .split("==")
             .collect::<Vec<_>>();
let right = parser[1].to_owned();
let left = parser[0].split('+').map(|s| s.to_owned()).collect();
```
## Second challenge: propagating Result and Option in a map
The challenge here is the _no leading 0_ requirement added an extra check to perform on the function to convert a string in a number.\
I initially opted to use and `Option::None`, but eventually settled on `Result` because `try_fold` **really** wanted a `Return` type.
Few learning points:
  * It is easy to convert a `Option` to `Result` and the other way around.
```rust
let result: Result<T,E> = some_option.ok_or(error);
let option: Option<T> = some_result.ok();
```
  * Finally got the difference between `for_each` and `map`:
    * `map` is lazyly evaluated and returns an iterator with the closure applied to every element
    * `for_each` is literally like a for loop: no lazy execution and it doesn't return the iterator
    * `fold` is the stateful version of `for_each`
    * there are versions with error handlying: `try_each` and `try_fold` but not for map :(

# [Two Bucket](https://exercism.io/my/solutions/3856e42afe1a458497ce741b59e3f39c)

## External crate used
[num](https://docs.rs/crate/num/0.3.1)
```rust
[dependencies]
num = "0.3"
```

Key method needed:
```rust
Integer::gcd(&self, other: impl Integer) 
```
Returns the GCD between 2 integers.

## Unit testing
It is possible to test internal methods / functions by adding a test module in the library file.
```rust
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1()
      ...
```

To run unit testing only, use 
```sh
cargo run --lib
```
Unfortunately not included in Rust extension for _IntelliJ_

# [Spiral Matrix](https://exercism.io/my/solutions/acacd9a10adc406495a2cb6779086a87)

Nothing major here...

# [Palindrome Product](https://exercism.io/my/solutions/226a512400d545429b22367c237ec448)

Nothing major here...

# [Saddle Points](https://exercism.io/my/solutions/3f8e31eededc41e5a6b4e884bb8cc4fc)

Getting easier and easier

# [Isograms](https://exercism.io/my/solutions/b23f44fc9431414799e5eeb911066c4d)

This one was EASY!

# [Say](https://exercism.io/my/solutions/90f99b87719a4f95a0bd3c4f37149373)

I really don't like strings, but I have learned how to make very basic macros!.

## Macros
I only needed a way to define a constant `String`, funny enough a `const` can't be a `String` because the type requires an allocation on the stack.
Enter macros.
```rust
macro_rules! zero {
    // zero!() -> String::from("Zero")
    () => { String::from("zero") }
}
```

## Match doesn't work with functions!
Somehow similar to the previous point, I discovered match doens't work with `String`, or rather with functions, but I was comparing a String to `String::from("zero")..
I'm sure I could have used string literal instead but that would have required additional restructuring.

# [Run Length Encoding](https://exercism.io/my/solutions/712ab0c9ff3b4e4cbaf2503085e8766e)

## String::parse

To convert a string to int:
```rust
let s = "133";
let n: Result<u32> = string.parse();
```
# [ISBN Verifier](https://exercism.io/my/solutions/8cf357cda7dc425b9993cbe22e0475e1)
This was the perfect opportunity to try regexp in Rust.

## Regexp
The official docs are pretty good if you already are familiar with Regexp.
```rust
let re = Regex::new(r"^(\d)-?(\d)(\d)(\d)-?(\d)(\d)(\d)(\d)(\d)-?([\dX])$").unwrap();
let caps = re.captures(isbn)?;
```

Note:
`caps` type is a `Capture` struct, because the groups can be optional, the interface for `get()` returns an `Option`.
You could also get an iterator over groups but it will start with the group 0 - aka the full match - which wasn't needed in this case.
```rust
for i in 1..=10 {
  let d = caps.get(i).unwrap().as_str();
}
```
# [Hamming](https://exercism.io/my/solutions/bff09aef03df4d4f83f2845516c58788)

Great opportunity to use Iterators - and the solution is really nice too.

```rust
distance = s1.chars()
        .zip(s2.chars())
        .filter(|(c1,c2)| c1 != c2)
        .count()
```

* `zip()`: very similar to Python, but is a method on the `Iterator` trait rather than a function.\
  It is used to pair elements in 2 Iterators, one by one.
  
* `count()`: surprisingly, counts the elemtn in an `Iterator`

# [Scrabble Score](https://exercism.io/my/solutions/a1e92dc5fd3f411195e0b411ad4d429f)
Half of the challenge is creating an easy API for the scoring table..
I used an `HashMap<char, u64>`.

Challenges encountered:
* converting a `String` of 1 ASCII char to `char`
```rust
string.split(", ") // e.g. "A, B, C, D"
  .map(|s| s.chars().next().unwrap()) // 'A' | 'B' | 'C' | 'D'
```              

* case insensitive lookup -> as the Table is built w capital letters, I forced the lookup to be uppercase
```rust
table.get(&c.to_ascii_uppercase()).unwrap_or(&0u64).to_owned()
```

# [Pangram](https://exercism.io/my/solutions/3dde8f0299044c27bdb57d3a5d688c34)

To iterate over range of `char`, you have to use bite literals and convert to char:
```rust
(b'a'..=b'z').map(char::from)
```

# [All your base](https://exercism.io/my/solutions/844d0d13fbe14924b447c5a96d7be8d4)

Nothing major other than the pretty comprehensive test!

# [Alergies](https://exercism.io/my/solutions/93facec67fd047769ca640ce83f461f4)

The trick was using bitwise operations as it is using bits to set a specific allergen.

## Iterate over Enum values
Rust doesn't have a built in way to do this, but you can use the [strum](https://crates.io/crates/strum) crate to easily iterate through the values of an enum.

```rust
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

#[derive(Debug, EnumIter)]
pub enum Allergen {
    Eggs,
    Peanuts,
    Shellfish,
    Strawberries,
    Tomatoes,
    Chocolate,
    Pollen,
    Cats,
}

for allergen in Allergen::iter() {
            println!("{:?}",allergen)
    } // Egg, Peanuts, She"llfish.. etc
```

# [Affine Cipher](https://exercism.io/my/solutions/3609937158bc4fcea49fe3faf4bf46d2)

## Reminder not Modulo!
In rust the `%` operator is the reminder operation.
What this algorithm needs is the modulo operator (the results are different for negative numbers) which is done using `rem_euclid`
```rust
let a: i32 = -21;
let b: i32 = 4;
a % b // -1
a.rem_euclid(b) // 3
```

## ASCII and bytes

I used the `ascii` crate to manipulate ascii strings.
The main `struct` I needed is `AsciiChar` which is literally an Ascii char, aka a character represented on 1 bit.
```rust
|c| AsciiChar::from_ascii(c).unwrap() // converts a char into AsciiChar (as the char was filtered for is_ascii earlier, this is safe)
```

Now I can manipulate as a byte:
```rust
let x = c.as_byte() - b'a' // ord of letter c
```

## Add a space every 5 characters
The method needed to do this in a functional way is `.chunks()`:
```rust
let s = "collatedstring".to_owned();
s.chunks(5).join(" ") // "colla tedst ring"
```
**Note:** `chunks()` need a vector, can't be applied on an iterator as it needs to save the values somewhere.

# [Variable Lenght Quantity](https://exercism.io/my/solutions/f480774be35041718a346e08d4f192e2)

Interesting exercise to experiment with bitwise operations.

## DequeVec
To encode a value, the algorithm starts with the right-most bit, that will be included in the last encoded byte.
This is a situation where you need to reverse a stack - I opted to use a `VecDeque`
```rust
let mut deque: VecDeque<u8> = VecDeque::new();
let mut stack: Vec<u8> = Vec::new();
stack.push(1);
stack.push(2);
stack // [1, 2]
deque.push_back(1);
deque.push_back(2); 
deque // deque : [1,2]
deque.push_front(0); 
deque // deque : [0,1,2]
```

## checked operations
Most operators are available in overflow safe methods with  `checked_<operation>()`
```rust
value = value.checked_shl(7) // <- I actually think this one is buggy as it didn't have the expected behaviour
```

Becase `checked_shl()` didn't have the behaviour I was expecting, I ended up checking for overflow manually before the operation.

# [Pig Latin](https://exercism.io/my/solutions/7953682ac86047d8be54c7554f3a9202)

 More regex, some were nastier than others to write.

# [Atbash Cipher](https://exercism.io/my/solutions/cec31295232c4def8595ba3c74653d2e)

Similar challenge to the Affine Cipher, but with a much easier encoding function.
