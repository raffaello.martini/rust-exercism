use std::collections::BTreeMap;
use std::collections::BTreeSet;

#[derive(Eq, PartialEq,Ord,PartialOrd,Debug)]
pub struct School {
    grades: BTreeMap<u32,Roster>,
}

#[derive(Eq, PartialEq,Ord,PartialOrd,Debug)]
pub struct Roster {
    grade: u32,
    students: BTreeSet<Student>,
}


impl Roster {
    fn insert(&mut self, student: Student) -> bool {
        self.students.insert(student)
    }

    fn with_grade(grade: u32) -> Self {
        Self {
            grade,
            students: BTreeSet::new(),
        }
    }
}

#[derive(Eq, PartialEq,Ord,PartialOrd,Debug)]
pub struct Student(String);

impl School {
    pub fn new() -> School {
        School {
            grades: BTreeMap::new(),
        }
    }

    pub fn add(&mut self, grade: u32, student: &str) {
        // unimplemented!("Add {} to the roster for {}", student, grade)
        let student = Student(student.to_string());
        if let Some(roster) = self.grades.get_mut(&grade) {
            roster.insert(student);
        } else {
            // empty roster for grade
            let mut roster = Roster::with_grade(grade);
            roster.insert(student);
            self.grades.insert(grade, roster);
        }
    }

    pub fn grades(&self) -> Vec<u32> {
        self.grades.keys().cloned().collect::<Vec<_>>()
    }

    // If grade returned an `Option<&Vec<String>>`,
    // the internal implementation would be forced to keep a `Vec<String>` to lend out.
    // By returning an owned vector instead,
    // the internal implementation is free to use whatever it chooses.
    pub fn grade(&self, grade: u32) -> Option<Vec<String>> {
        // unimplemented!("Return the list of students in {}", grade)
        let roster = self.grades.get(&grade)?;
        Some(roster
            .students
            .iter()
            .map(|student| {
                student.0.clone()
            })
            .collect::<Vec<_>>())
    }
}
