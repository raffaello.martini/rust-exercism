pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    // "Sum the multiples of all of {:?} which are less than {}"

    (0..limit) // loop through all numbers up to limit
        .filter(|x|
            factors.iter()
            .filter(|x| **x != 0)// remove 0 from factor (0 is only multiple ot itself)
            .any(|y| x % y == 0))// include numbers multiple of any factor
        .sum()
}


/*
Learning points:
  * comparison operators don't deref

*/