enum MessageType {
    Question,
    Yell,
    YellQuestion,
    Silence,
    Whatever,
}

fn parse(message: &str ) -> MessageType {

    // remove trailing whitespaces
    let message = message.trim();

    // question
    let is_question =  match message.chars().last() {
        Some('?') => true,
        _ => false,
    };

    // all literal characters are ALL CAP
    let is_all_cap = message == message.to_uppercase() && message != message.to_lowercase();

    // blank string
    let is_blank= message.is_empty();

    match (is_question, is_all_cap) {
        (true,true) => MessageType::YellQuestion,
        (true,false) => MessageType::Question,
        (false,true) => MessageType::Yell,
        _ => if is_blank { MessageType ::Silence }
                else { MessageType::Whatever}
    }
}

pub fn reply(message: &str) -> &str {
    let parsed: MessageType = parse(message);
    match parsed {
        MessageType::Question => "Sure.",

        // Yell - all CAPS
        MessageType::Yell => "Whoa, chill out!",

        //  Question - ends w question mark
        MessageType::YellQuestion => "Calm down, I know what I'm doing!",

        // Whitespaces
        MessageType::Silence => "Fine. Be that way!",

        // catch all
        MessageType::Whatever => "Whatever.",
    }
}

/*
Learning Points

  * String and &str are tedious

*/




