#[derive(Debug, PartialEq)]
pub struct Dna {
    dna: Vec<char>,
}

#[derive(Debug, PartialEq)]
pub struct Rna {
    rna: Vec<char>,
}

impl Dna {
    pub fn new(dna: &str) -> Result<Dna, usize> {
        let dna: Result<Vec<_>, _> = dna
            .chars()
            .enumerate()
            .map(|(i, n)| {
                match n {
                    'G' | 'C' | 'T' | 'A' => Ok(n),
                    _ => Err(i),
                }
            })
            .collect();
        Ok(Self {
            dna: dna?,
        })
        // unimplemented!("Construct new Dna from '{}' string. If string contains invalid nucleotides return index of first invalid nucleotide", dna);
    }

    pub fn into_rna(self) -> Rna {
        let rna = self.dna
            .iter()
            .map(|n| match n {
                'G' => 'C',
                'C' => 'G',
                'T' => 'A',
                'A' => 'U',
                _ => '_',
            }).collect();
        Rna {rna}
    }
}

impl Rna {
    pub fn new(rna: &str) -> Result<Rna, usize> {
        let rna: Result<Vec<_>, _> = rna
            .chars()
            .enumerate()
            .map(|(i, n)| {
                match n {
                    'C' | 'G' | 'A' | 'U' => Ok(n),
                    _ => Err(i),
                }
            })
            .collect();
        Ok(Self {
            rna: rna?,
        })
    }
}
