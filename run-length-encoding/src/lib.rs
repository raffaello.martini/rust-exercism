struct EncoderHelper {
    c: char,
    repetition: usize,
}

impl EncoderHelper {
    fn new() -> Self {
        EncoderHelper {
            c: '?',
            repetition: 0,
        }
    }

    fn parse(&mut self, c: char) -> Option<String> {
        if c.is_numeric() {
            panic!("Can not encode num chars");
        }
        //     first character
        if self.repetition == 0 {
            self.c = c;
            self.repetition = 1;
        } else if c == self.c {
            //     previous character == c
            self.repetition += 1;
        } else {
            //     previous character != c
            let sequence = if self.repetition == 1 { format!("{}", self.c) } else { format!("{}{}", self.repetition, self.c) };
            self.c = c;
            self.repetition = 1;
            return Some(sequence);
        }
        None
    }

    fn finally(&mut self) -> String {
        if self.repetition == 0 {
            "".to_string()
        } else if self.repetition == 1 { format!("{}", self.c) } else { format!("{}{}", self.repetition, self.c) }
    }
}

pub fn encode(source: &str) -> String {
    let mut result = String::default();
    let mut helper = EncoderHelper::new();
    for c in source.chars() {
        let parser = helper.parse(c);
        if let Some(string) = parser {
            result = format!("{}{}", result, string);
        }
    }
    let parser = helper.finally();
    format!("{}{}", result, parser)
}

// enum Status{
//     Init,
//     Normal,
// }

struct DecodeHelper {
    rep: String,
}

impl DecodeHelper {
    fn new() -> Self {
        Self {
            rep: String::default(),
        }
    }

    fn decode(&mut self, c: char) -> Option<String> {
        if c.is_numeric() {
            self.rep = format!("{}{}", self.rep, c);
            None
        } else {
            //     caracter repeated X times
            if self.rep == String::default() {
                //     empty string - single repetition
                Some(format!("{}", c))
            } else {
                //     non empty string - there is a repetition
                let rep = self.rep.parse().unwrap();
                Some(format!("{}", c.to_string().repeat(rep)))
            }
        }
    }
}

pub fn decode(source: &str) -> String {
    let mut helper = DecodeHelper::new();
    let mut result = String::default();
    for c in source.chars() {
        if let Some(s) = helper.decode(c) {
            result = format!("{}{}",result,s);
            helper = DecodeHelper::new();
        }
    }
    result
}
