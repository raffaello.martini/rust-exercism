extern crate ascii;
extern crate num;

use ascii::AsciiChar;

use crate::AffineCipherError::NotCoprime;

/// While the problem description indicates a return status of 1 should be returned on errors,
/// it is much more common to return a `Result`, so we provide an error type for the result here.
#[derive(Debug, Eq, PartialEq)]
pub enum AffineCipherError {
    NotCoprime(i32),
}

/// Encodes the plaintext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn encode(plaintext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    if !is_coprime(a, 26) {
        return Err(NotCoprime(a));
    }
    let result = plaintext.chars()
        .filter(char::is_ascii_alphanumeric)
        .map(|c| {
            c.to_ascii_lowercase()
        })
        .map(|c| AsciiChar::from_ascii(c).unwrap())
        .map(|cc| {
            if cc >= b'a' && cc <= b'z' {
                let x = (cc.as_byte() - b'a') as i32;
                (a * x + b).rem_euclid(26) as u8 + b'a'
            } else {
                cc.as_byte()
            }
        })
        .collect::<Vec<u8>>()
        .chunks(5)
        .map(|s| { String::from_utf8(s.to_vec()).unwrap() })
        .collect::<Vec<_>>()
        .join(" ");
    Ok(result)
}

/// Decodes the ciphertext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn decode(ciphertext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    if !is_coprime(a, 26) {
        return Err(NotCoprime(a));
    }
    let mmi = mmi(a, 26);
    let result = ciphertext.chars()
        .filter(|c| !c.is_whitespace())
        .map(|c| AsciiChar::from_ascii(c).unwrap())
        .map(|cc| {
            if cc >= b'a' && cc <= b'z' {
                //     c is a letter
                let y = (cc.as_byte() - b'a') as i32;
                (mmi * (y - b)).rem_euclid(26) as u8 + b'a'
            } else {
                //     c is not a letter
                cc.as_byte()
            }
        })
        .collect::<Vec<u8>>();
    Ok(String::from_utf8(result).unwrap())
}

fn is_coprime(a: i32, b: i32) -> bool {
    use num::integer::gcd;
    gcd(a, b) == 1
}

fn mmi(a: i32, m: i32) -> i32 {
    // https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
    let mut t = (0, 1);
    let mut r = (m, a);

    while r.1 != 0 {
        let q = r.0 / r.1;
        t = (t.1, t.0 - q * t.1);
        r = (r.1, r.0 - q * r.1);
    }

    if r.0 > 1 {
        panic!("a is not invertible");
    }

    if t.0 < 0 {
        t.0 = t.0 + m
    }

    return t.0;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn coprime() {
        assert!(is_coprime(5, 26))
    }

    #[test]
    fn simple_encode() {
        let text = "test".to_owned();
        let expected = "ybty".to_owned();
        assert_eq!(expected, encode(&text, 5, 7).unwrap());
    }

    #[test]
    fn longer_encode() {
        let text = "test123".to_owned();
        let expected = "ybty1 23".to_owned();
        assert_eq!(expected, encode(&text, 5, 7).unwrap());
    }

    #[test]
    fn very_long_encode() {
        let text = "testtesttesttest".to_owned();
        let expected = "ybtyy btyyb tyybt y".to_owned();
        assert_eq!(expected, encode(&text, 5, 7).unwrap());
    }

    #[test]
    fn check_mmi() {
        assert_eq!(3, mmi(9, 26));
        assert_eq!(7, mmi(15, 26))
    }

    #[test]
    fn simple_decode() {
        let ciphertext = "ybty".to_owned();
        let expected = "test".to_owned();
        assert_eq!(expected, decode(&ciphertext, 5, 7).unwrap());
    }

    #[test]
    fn longer_decode() {
        let ciphertext = "ybtyy btyyb tyybt y".to_owned();
        let expected = "testtesttesttest".to_owned();
        assert_eq!(expected, decode(&ciphertext, 5, 7).unwrap());
    }

    #[test]
    fn decode_all_the_letters() {
        println!("mmi {}", mmi(17, 26));
    }
}
