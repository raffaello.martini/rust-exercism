pub fn find<A, T>(array: A, key: T) -> Option<usize>
    where
        A: AsRef<[T]>,
        T: Ord,
{
    // let slice = array.as_ref();
    let array = array.as_ref();
    if array.len() == 0 || (array.len() == 1 && key != array[0]) {
        // last element
        return None;
    }
    let mid = array.len() / 2;
    if key == array[mid] {
        Some(mid)
    } else if key < array[mid] {
        find(&array[0..mid], key)
    } else {
        Some(find(&array[mid..], key)? + mid)
    }
}
