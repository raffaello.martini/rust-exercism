use std::collections::BTreeSet;

#[derive(Debug, Clone)]
struct Sieve {
    flagged: BTreeSet<u64>,
    upper_bound: u64,
}

impl Sieve {
    fn with_capacity(upper_bound: u64) -> Self {
        Self {
            flagged: BTreeSet::new(),
            upper_bound,
        }
    }

    fn next_unmarked(&self, current: u64) -> Option<u64> {
        // &self.0[current as usize ..]
        (current + 1..=self.upper_bound)
            .find(|x| !self.flagged.contains(x))
    }
}


pub fn primes_up_to(upper_bound: u64) -> Vec<u64> {
    let mut sieve = Sieve::with_capacity(upper_bound);
    let mut result = Vec::new();
    let mut current = 2;
    while current <= upper_bound {
        // current is a prime number
        result.push(current);
        // flag all multiples of the prime number
        for m in (1..).take_while(|m| m * current <= upper_bound) {
            sieve.flagged.insert(m * current);
        }
        if let Some(next) = sieve.next_unmarked(current) {
            current = next;
        } else {
            break;
        }
    }
    result
}
