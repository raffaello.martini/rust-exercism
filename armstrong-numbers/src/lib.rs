pub fn is_armstrong_number(num: u32) -> bool {
    // unimplemented!("true if {} is an armstrong number", num)
    let digits = to_digits(num);
    let n = digits.len() as u32;
    let test: u32 = digits.iter()
        .map(|x| x.pow(n))
        .sum();
    test == num
}

fn to_digits(num: u32) -> Vec<u32> {
    // let mut digits :Vec<u32> = vec![];
    // while num != 0 {
    //     digits.insert(0, num % 10);
    //     num /= 10;
    // }
    // digits
    match num {
        0 => vec![],
        _ => [to_digits(num / 10), vec![num % 10]].concat(),
    }
}

/*
Learning points:
  * need to review ways of concatenating vector
  * enum / len etx return usize

 */