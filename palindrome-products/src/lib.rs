#[derive(Debug, PartialEq, Eq)]
pub struct Palindrome {
    value: u64,
    factors: Vec<(u64,u64)>,
    // implement your palindrome type here
}

fn is_palindrome(a: u64, b: u64) -> bool{
    let p_string = (a * b).to_string();
    p_string == p_string.chars().rev().collect::<String>()
}

impl Palindrome {
    pub fn new(a: u64, b: u64) -> Palindrome {
        let mut factors = Vec::new();
        factors.push((a,b));
        Palindrome{
            value: a * b,
            factors
        }
    }

    pub fn value(&self) -> u64 {
        self.value
    }

    pub fn insert(&mut self, a: u64, b: u64) {
        self.factors.push((a,b));
    }
}

pub fn palindrome_products(min: u64, max: u64) -> Option<(Palindrome, Palindrome)> {
    let mut result:Option<(Palindrome, Palindrome)> = None;
    for a in min..=max {
        for b in a..=max {
            if is_palindrome(a, b) {
                if let Some((mut min, mut max)) = result {
                    let prod = a * b;
                    if prod < min.value() {
                        min = Palindrome::new(a, b);
                    } else if prod == min.value() {
                        min.insert(a,b);
                    }
                    if prod > max.value() {
                        max = Palindrome::new(a, b);
                    }   else if prod == max.value() {
                        max.insert(a,b);
                    }
                    result = Some((min, max));
                }  else {
                    result = Some((
                        Palindrome::new(a,b),
                        Palindrome::new(a,b)));
                }
            }
        }
    }
    result
}
