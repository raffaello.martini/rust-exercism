pub fn build_proverb(list: &[&str]) -> String {
    if list.len() == 0 {
        return "".to_string();
    }
    let staggered = &list[1..];
    list.iter().zip(staggered) //staggered pairs "nail","shoe" / "shoe","horse" and so on
        .map(|(x, y)| format!("For want of a {} the {} was lost.",x,y))
        .chain(std::iter::once(format!("And all for the want of a {}.", list[0])))
        .collect::<Vec<String>>()
        .join("\n")
}

/*
Learning points:
  * can't return &str, because the str has to be stored somewhere, return String
  * slices and iter are similar to Python (but more painful to deal with)
  * appending strings in Rust is HARD, specifically in this case I had an immutable Vec of String,
    appending a String to it isn't a trivial matter
  * ste::iter::once() to create an iterator pointing to a single element from a variable
 */
