pub fn raindrops(n: u32) -> String {
    let mut result= "".to_string();
    let pling = n % 3 == 0;
    let plang = n % 5 == 0;
    let plong = n % 7 == 0;

    if !pling && !plang && !plong {
        result = format!("{}",n);
    } else {
        if pling {
            result = "Pling".to_string();
        }
        if plang {
            result = format!("{}Plang",&result);
        }
        if plong {
            result = format!("{}Plong",&result);
        }
    }
    result
}
