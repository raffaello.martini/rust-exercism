#[derive(Copy, Clone, PartialEq, Debug)]
enum Direction {
    E,
    S,
    W,
    N,
}

#[derive(Copy, Clone, PartialEq, Debug)]
struct Position(usize, usize, Direction);

impl Position {
    fn origin() -> Self {
        Position(0, 0, Direction::E)
    }

    fn next(&self, size: u32) -> Self {
        use Direction::*;
        let max = size - 1;
        let &Position(i, j, d) = self;
        match d {
            E => {
                if j < max as usize{
                    Position(i, j + 1, E)
                } else { self.turn() }
            }
            S => {
                if i < max as usize{
                    Position(i + 1, j, S)
                } else {
                    self.turn()
                }
            }
            W => {
                if j > 0 {
                    Position(i, j - 1, W)
                } else {
                    self.turn()
                }
            }
            N => {
                if i > 0 {
                    Position(i - 1, j, N)
                } else {
                    self.turn()
                }
            }
        }
    }

    fn turn(&self) -> Self {
        use Direction::*;
        let &Position(i, j, d) = self;
        match d {
            E => Position(i + 1, j, S),
            S => Position(i, j - 1, W),
            W => Position(i - 1, j, N),
            N => Position(i, j + 1, E)
        }
    }
}

pub fn spiral_matrix(size: u32) -> Vec<Vec<u32>> {
    let mut matrix = Vec::new();
    if size == 0 {
        return matrix
    } else if size == 1 {
        matrix.push(vec![1]);
        return matrix
    }
    for _i in 0..size {
        matrix.push(vec![0; size as usize]);
    }
    let mut pos = Position::origin();
    matrix[0][0] = 1;
    for n in 2..=size.pow(2) {
        let Position(mut i, mut j, dir) = pos.next(size);
        if matrix[i][j] == 0 {
            //     not assigned number
            pos = Position(i, j, dir);
        } else {
            // already assigned, turn
            pos = pos.turn();
            i = pos.0;
            j = pos.1;
        }
        matrix[i][j] = n;
    }
    matrix
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_next() {
        let pos = Position::origin();
        assert_eq!(pos.next(4), Position(0, 1, Direction::E));
        let pos = Position(1, 3, Direction::E);
        assert_eq!(pos.next(4), Position(2, 3, Direction::S));
        let pos = Position(3, 3, Direction::S);
        assert_eq!(pos.next(4), Position(3, 2, Direction::W));
        let pos = Position(3, 3, Direction::W);
        assert_eq!(pos.next(4), Position(3, 2, Direction::W));
        assert_eq!(Position(3, 0, Direction::W).next(4), Position(2, 0, Direction::N));
        assert_eq!(Position(0, 0, Direction::N).next(4), Position(0, 1, Direction::E));
    }
}
