#[derive(PartialEq, Debug, Copy, Clone)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(PartialEq, Debug, Copy, Clone)]
pub struct Robot {
    x: i32,
    y: i32,
    direction: Direction,
}

impl Robot {
    pub fn new(x: i32, y: i32, d: Direction) -> Self {
        Self {x, y, direction: d}
    }

    pub fn turn_right(self) -> Self {
        use Direction::*;
        let direction = match self.direction {
            North => East,
            East => South,
            South => West,
            West => North,
        };
        Self {
            x: self.x,
            y: self.y,
            direction,
        }
    }

    pub fn turn_left(self) -> Self {
        use Direction::*;
        let direction = match self.direction {
            North => West,
            West => South,
            South => East,
            East => North,
        };
        Self {
            x: self.x,
            y: self.y,
            direction,
        }
    }

    pub fn advance(self) -> Self {
        use Direction::*;
        let (mut x,mut y) = (self.x, self.y);
        let direction = self.direction;
        match direction {
            North => {y += 1}
            East => {x += 1}
            South => {y -= 1}
            West => {x -= 1}
        };
        Self { x, y, direction }
    }

    pub fn instructions(self, instructions: &str) -> Self {
        instructions
            .chars()
            .fold(self,
                |robot, i| {
                    match i {
                        'A' => robot.advance(),
                        'L' => robot.turn_left(),
                        'R' => robot.turn_right(),
                        _ => robot // if the instruction is not correct, skip to next
                    }
                }
            )
    }

    pub fn position(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    pub fn direction(&self) -> &Direction {
        &self.direction
    }
}
