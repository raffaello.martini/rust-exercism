pub mod graph {
    // use std::collections::HashMap;
    use crate::graph::graph_items::node::Node;
    use crate::graph::graph_items::edge::Edge;
    use crate::graph::attributes::{Attribute, Attributes};
    use std::collections::HashMap;

    pub struct Graph {
        pub nodes: Vec<Node>,
        pub edges: Vec<Edge>,
        pub attrs: Attributes,
    }

    impl Graph {
        pub fn new() -> Self {
            Graph {
                nodes: Vec::new(),
                edges: Vec::new(),
                attrs: Attributes::new(),
            }
        }

        pub fn with_nodes(mut self, nodes: & Vec<Node>) -> Graph {
            for node in nodes.clone() {
                self.nodes.push(node);
            }
            self
        }

        pub fn with_edges(mut self, edges: & Vec<Edge>) -> Graph {
            for edge in edges.clone() {
                self.edges.push(edge);
            }
            self
        }

        pub fn with_attrs(mut self, attrs: &[Attribute]) -> Graph {
            self.attrs = self.attrs.with_attrs(attrs);
            self
        }

        pub fn get_node(self, name: &str) -> Option<Node>{
            for node in self.nodes.clone() {
                if node.name() == name {
                    return Some(node)
                }
            }
            None
        }

        pub fn attrs(&self) -> &HashMap<String,String> {
            &self.attrs.attrs
        }
    }

    pub mod graph_items {
        // use std::collections::HashMap;

        pub mod node {
            // use std::collections::HashMap;
            use crate::graph::attributes::{Attribute, Attributes};

            #[derive(Clone, PartialEq, Debug)]
            pub struct Node {
                name: String,
                attrs: Attributes,
            }

            impl Node {
                pub fn new(name: &str) -> Self {
                    Node {
                        name: name.to_string(),
                        attrs: Attributes::new(),
                        // attrs: HashMap,
                    }
                }

                pub fn with_attrs(mut self, attrs: &[Attribute]) -> Node {
                    self.attrs = self.attrs.with_attrs(attrs);
                    self
                }

                pub fn name(&self) -> &str {
                    self.name.as_str()
                }

                pub fn get_attr(&self, attr: &str) -> Option<&str> {
                    self.attrs.get_attr(attr)
                }
            }



            // pub fn get_attr(&self, attr: &str) -> Option<&str> {
            //
            //     match self.attrs.get(attr) {
            //         Some(attr) => Some(attr.as_str()),
            //         _ => None,
            //     }
        }

        pub mod edge {
            // use std::collections::HashMap;
            use crate::graph::attributes::{Attribute, Attributes};

            #[derive(Clone, Debug, PartialEq)]
            pub struct Edge {
                src_node: String,
                dst_node: String,
                attrs: Attributes,
            }

            impl Edge {
                pub fn new(src_node: &str, dst_node: &str) -> Self {
                    Edge {
                        src_node: src_node.to_string(),
                        dst_node: dst_node.to_string(),
                        attrs: Attributes::new(),
                    }
                }

                pub fn with_attrs(self, attrs: &[Attribute]) -> Self {
                    Edge {
                        attrs: self.attrs.with_attrs(attrs),
                        ..self
                    }
                }
            }
        }
    }

    mod attributes {
        use std::collections::HashMap;

        pub type Attribute<'a> = (&'a str, &'a str);

        #[derive(Clone, PartialEq, Debug)]
        pub struct Attributes{
            pub attrs: HashMap<String, String>,
        }

        impl Attributes {
            pub fn new() -> Self {
                Attributes{
                    attrs : HashMap::new(),
                }
            }

            pub fn with_attrs(mut self, attrs: &[Attribute]) -> Self {
                self.attrs = attrs
                    .iter()
                    .map(|(k, v)| (String::from(*k), String::from(*v)))
                    .collect();
                self
            }

            pub fn get_attr(&self, attr: &str) -> Option<&str> {
                match self.attrs.get(attr) {
                    Some(attr) => Some(attr.as_str()),
                    _ => None,
                }
            }

            pub fn is_empty(self) -> bool {
                self.attrs.is_empty()
            }
        }
    }
}

/*

Learning points:

  * 2 flavor of Builder pattern, I had to use the consuming pattern to allow one-liner configuration
  * easier using String than &str for Hash map key
    * Hashmap<String, _>::get(x) allows x both as &str and String

  * "Inheritance"
    * Rust doesn't have inheritance like a OOP language
    * Trait to define common behaviour across Struct, they serve the purpose of interfaces in Java
    * To extend a struct, you can nest a base structure in other structures
    * [https://users.rust-lang.org/t/how-to-implement-inheritance-like-feature-for-rust/31159]
    * [Is rust an OOP language] [https://users.rust-lang.org/t/is-rust-oop-if-not-what-is-it-oriented-to/30777/6]
    * Rust Koans [https://users.rust-lang.org/t/rust-koans/2408]

  * Modules
    * A crate can have multiple modules
    * modules can have submodules
    * every module or submodule can be public or private
    * an item (function, method, structure, Trait etc) has to be pub from the rood down to be externally available
    * a module has access to pub items under a private submodule (example attributes)
    * visibility modifiers!!!


 */

