use std::collections::VecDeque;

#[derive(Debug, PartialEq)]
pub enum Error {
    IncompleteNumber,
    Overflow,
}

/// Convert a list of numbers to a stream of bytes encoded with variable length encoding.
pub fn to_bytes(values: &[u32]) -> Vec<u8> {
    values.iter()
        .flat_map(|v| {
            value_to_bytes(v).into_iter()})
        .collect()
}

fn value_to_bytes( value: &u32) -> Vec<u8> {
    let mut result: VecDeque<u8> = VecDeque::new();
    let mut x = *value;
    let mut last = true;
    loop {
        let mut byte = (x & 127) as u8;
        if !last {
            byte = byte | 128;
        }
        result.push_front(byte);
        x = x >> 7;
        last = false;
        if x == 0 { break; }
    }
    result.into_iter().collect()
}

/// Given a stream of bytes, extract all numbers which are encoded in there.
pub fn from_bytes(bytes: &[u8]) -> Result<Vec<u32>, Error> {
    let mut value = 0u32;
    let mut result: Vec<u32> = Vec::new();
    let mut is_complete = false;
    for b in bytes {
        is_complete = false;
        let b = *b;
        value += (b & 127) as u32;
        if b & 128 == 0 {
            //      last byte
            result.push(value);
            value = 0;
            is_complete = true;
        } else {
            if value & 0xfe000000 != 0 {
                // if any of the first 7 bit is 1 it will overflow
                return Err(Error::Overflow);
            }
            value = value << 7;
        }
    }
    if is_complete {
        Ok(result)
    } else {
        Err(Error::IncompleteNumber)
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_value_to_bytes() {
        let zero = vec![0u8];
        assert_eq!(value_to_bytes(&0), zero);
        let x40 = vec![0x40u8];
        assert_eq!(value_to_bytes(&0x40), x40);
        let x2000 = vec![0xc0u8, 0x00u8];
        assert_eq!(value_to_bytes(&0x2000), x2000);
    }
}
