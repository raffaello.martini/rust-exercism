pub fn abbreviate(phrase: &str) -> String {
    phrase
        .split(|c: char| c.is_whitespace() || c.is_ascii_punctuation() && c != '\'')// break down in words
        .map(|s| {
            let first_char = s
                .get(..1) // takes the first letter
                .map_or("".to_string(), |c| c.to_uppercase()); // makes it uppercase if successful, or empty string
            let other = s
                .get(1..) // takes all other letters
                .map_or("".to_string(), |other|  // failsafe unwrap
                    format!(" {}", other) // add a whitespace for the ALL UPPERCASE word
                        .split(|c: char| c.is_lowercase() || c.is_ascii_punctuation()) // the APOSTROPHE has a different rule from punctuation
                        .map(|camelcase_word| {
                            camelcase_word
                                .get(..1) // get first letter
                                .unwrap_or("")
                                .trim()
                                .to_string()
                        })
                        .collect()); // deals with single word camelcase
            first_char + &other
        })
        .collect()
}


/*
Note: so many corner cases!
This solution will still fail with weird camelcases like "aRt bART cARt"
 */