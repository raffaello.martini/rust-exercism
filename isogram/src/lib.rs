use std::collections::HashSet;

pub fn check(candidate: &str) -> bool {

    let only_alpha :String = candidate.chars().
        filter(|c| c.is_ascii_alphabetic())
        .map(|c| c.to_ascii_lowercase())
        .collect();
    only_alpha.chars().collect::<HashSet<_>>().len() == only_alpha.len()
}
