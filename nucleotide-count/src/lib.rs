use std::collections::HashMap;

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    match nucleotide {
        'A' | 'C' | 'G' | 'T' => {
            dna.chars().try_fold(0, |count, elem| {
                match elem {
                    'A' | 'C' | 'G' | 'T' => {
                        if nucleotide == elem { Ok(count + 1) }
                        else { Ok(count) }
                    }
                    _ => Err(elem)
                }
            })
        },
        _ => Err(nucleotide),
    }
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    let mut count = HashMap::with_capacity(4);
    count.insert('A', 0);
    count.insert('C', 0);
    count.insert('G', 0);
    count.insert('T', 0);
    dna.chars().try_for_each(|elem|{
        if let 'A' | 'C' | 'G' | 'T' = elem {
            // *count.entry(elem).or_insert(0usize) += 1;
            *count.get_mut(&elem).unwrap() += 1;
            Ok(())
        }
        else { Err(elem) }
    })?;
    Ok(count)
}
