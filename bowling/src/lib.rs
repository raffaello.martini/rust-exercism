use Error::*;
// use crate::Frame::{Spare, Complete};

#[derive(Debug, PartialEq)]
pub enum Error {
    NotEnoughPinsLeft,
    GameComplete,
}


#[derive(Copy, Clone, PartialEq)]
enum FrameState {
    Complete,
    Ongoing,
    Last,
    TwoExtraBall,
    OneExtraBall,
}

impl FrameState {
    fn strike(&self) -> Self {
        match self {
            Self::Ongoing => Self::Complete,
            Self::Last => Self::TwoExtraBall,
            Self::TwoExtraBall => Self::OneExtraBall,
            Self::OneExtraBall => Self::Complete,
            _ => Self::Complete // Shouldn't happen
        }
    }

    fn spare(&self) -> Self {
        match self {
            Self::Ongoing => Self::Complete,
            Self::Last => Self::OneExtraBall,
            _ => Self::Complete // Shouldn't happen
        }
    }

    fn open(&self) -> Self {
        // an open frame is always complete
        Self::Complete
    }

    fn first_ball(&self) -> Self {
        if let Self::OneExtraBall = self {
            return Self::Complete;
        }
        if let Self::TwoExtraBall = self {
            return Self::Ongoing;
        }
        self.clone()
    }
}

#[derive(Copy, Clone)]
enum Ball {
    Init,
    First(u16),
    Open(u16),
    Spare(u16),
    Strike,
}

impl Ball {
    fn value(&self) -> u16 {
        use Ball::*;
        match *self {
            First(b) | Spare(b) | Open(b) => b,
            Strike => 10,
            Init => 0,
        }
    }
}

#[derive(Copy, Clone)]
struct Frame {
    state: FrameState,
    ball: Ball,
    // is_complete: bool,
    frame_number: usize,
    standing_pins: u16,
}

impl Frame {
    fn start_n(frame_number: usize) -> Self {
        Self {
            state: if frame_number == 10 { FrameState::Last } else { FrameState::Ongoing },
            ball: Ball::Init,
            standing_pins: 10,
            frame_number,
        }
    }

    fn advance(&mut self) {
        let frame_number = self.frame_number + 1;
        *self = Frame::start_n(frame_number);
    }

    fn is_complete(&self) -> bool {
        self.state == FrameState::Complete
    }

    fn roll(&mut self, pins: u16) -> Result<Ball, Error> {
        if pins > self.standing_pins {
            return Err(Error::NotEnoughPinsLeft);
        }
        let ball = match self.ball {
            Ball::Init => {
                if pins == 10 {
                    self.state = self.state.strike();
                    Ball::Strike
                } else {
                    self.state = self.state.first_ball();
                    self.standing_pins -= pins;
                    Ball::First(pins)
                }
            }
            Ball::First(_) => {
                if pins == self.standing_pins {
                    self.state = self.state.spare();
                    Ball::Spare(pins)
                } else {
                    self.state = self.state.open();
                    Ball::Open(pins)
                }
            }
            _ => Ball::Init,
        };
        self.ball = match self.state {
            FrameState::OneExtraBall => {
// reset pins
                self.standing_pins = 10;
                Ball::First(0)
            }
            FrameState::TwoExtraBall => {
// reset pins
                self.standing_pins = 10;
                Ball::Init
            }
            _ => ball,
        };
        Ok(ball)
    }
}


pub struct BowlingGame {
    balls: Vec<Ball>,
    frame: Frame,
    is_complete: bool,
}

impl BowlingGame {
    pub fn new() -> Self {
        Self {
            balls: Vec::new(),
            frame: Frame::start_n(1),
            is_complete: false,
        }
    }

    pub fn roll(&mut self, pins: u16) -> Result<(), Error> {
        if self.is_complete {
            return Err(GameComplete);
        }
        let ball = self.frame.roll(pins)?;
        self.balls.push(ball);
        if self.frame.is_complete() {
            if self.frame.frame_number == 10 {
                self.is_complete = true;
            } else {
                self.frame.advance();
            }
        }
        Ok(())
    }

    pub fn score(&self) -> Option<u16> {
        use Ball::*;
        use std::iter::repeat;
// # pattern to cycle to a vector in a staggered way.
// # v[0..2]
// # v[1..3]
// # ...
// # v[n-2..n]
// {v.iter()
//  .zip(v[1..10].iter()
//      .chain(iter::repeat(&0i32)))
//  .zip(v[2..10].iter()
//      .chain(iter::repeat(&0i32)))
//  .collect::<Vec<_>>()}
        if !self.is_complete {
            return None;
        }
        let mut score = 0;
        let mut frame = 1;
        for ((current, next_1), next_2) in self.balls.iter()
            .zip(self.balls[1..].iter().chain(repeat(&Ball::Init)))
            .zip(self.balls[2..].iter().chain(repeat(&Ball::Init)))
        {
            if frame <= 10 {
                score += current.value();
                match current {
                    Spare(_) => score += next_1.value(),
                    Strike => score += next_1.value() + next_2.value(),
                    _ => (),
                }
            }
            if let Spare(_) | Open(_) | Strike = current {
                frame += 1;
            }
        }
        Some(score)
    }
}
