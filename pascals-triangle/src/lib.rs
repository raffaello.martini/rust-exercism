pub struct PascalsTriangle(u32);

impl PascalsTriangle {
    pub fn new(row_count: u32) -> Self {
        Self(row_count)
    }

    pub fn rows(&self) -> Vec<Vec<u32>> {
        let mut rows: Vec<Vec<u32>> = Vec::new();
        if self.0 == 0 {
            return rows;
        }
        rows.push(vec![1]);
        for row_n in 0..(self.0 - 1) as usize {
            let row = row(&rows[row_n]);
            rows.push(row);
        }
        rows
    }
}

fn row(row: &[u32]) -> Vec<u32> {
    let left = [&[0], row].concat();
    let right = [row, &[0]].concat();
    left.iter().zip(right).map(|(l, r)| l + r).collect()
}