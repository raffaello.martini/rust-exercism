use itertools::Itertools;
use std::collections::HashMap;
use std::collections::HashSet;

//
//     let perms = (0..10).permutations(#letters);

struct Parser {
    left: Vec<String>,
    right: String,
    letters: Vec<char>,
}

fn word_to_num(word: &str, map: &HashMap<char, u8>) -> Result<u64, String> {
    let mut num = 0;
    for (i, c) in word.chars().enumerate() {
        num = 10 * num + *map.get(&c).unwrap() as u64;
        // test 0 on first digit
        if i == 0 && num == 0 {
            return Err("Leading zero".to_owned());
        }
    }
    Ok(num)
}

impl Parser {
    fn parse(input: &str) -> Self {
        let set: HashSet<char> = input.chars().filter(|c| c.is_ascii_alphabetic()).collect();
        let letters = set.iter().map(char::to_owned).collect();
        let input = input
            .chars()
            .filter(|c| !c.is_whitespace())
            .collect::<String>();
        let parser = input
            .split("==")
            // .map(|s| s.to_string())
            .collect::<Vec<_>>();
        let right = parser[1].to_owned();
        let left = parser[0].split('+').map(|s| s.to_owned()).collect();
        Self {
            left,
            right,
            letters,
        }
    }

    fn solve(&self) -> Option<HashMap<char, u8>> {
        for perm in (0..10).permutations(self.letters.len()) {
            let result = self
                .letters
                .iter()
                .map(char::to_owned)
                .zip(perm.iter().map(u8::to_owned))
                .collect();
            let left: Result<u64, String> = self
                .left
                .iter()
                .map(|s| word_to_num(s, &result))
                .try_fold(0, |acc, x| Ok(acc + x?));
            let left = left.ok();
            let right = word_to_num(&self.right, &result).ok();
            if let (Some(left), Some(right)) = (left, right) {
                if left == right {
                    return Some(result);
                }
            }
        }
        None
        // SolverHelper::new(&self).solve()
    }
}

pub fn solve(input: &str) -> Option<HashMap<char, u8>> {
    let parser = Parser::parse(input);
    parser.solve()
}
