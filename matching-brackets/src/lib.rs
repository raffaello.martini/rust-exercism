use std::str::Chars;

enum State{
    Parentheses,
    Brackets,
    Braces,
    None,
    Missmatch,
}

pub fn brackets_are_balanced(string: &str) -> bool {
    // unimplemented!(
    //     "Check if the string \"{}\" contains balanced brackets",
    //     string
    // );
    let mut stack = vec![State::None];
    let stack = parse(string.chars(), &mut stack);
    match stack.last() {
        Some(State::None) => true,
        _ => false,
    }
}

fn parse<'a>(mut iter: Chars,  stack: &'a mut Vec<State>) -> &'a Vec<State> {
    if let Some(state) = stack.last() {
        match (state, iter.next()) {
            (_, Some('{')) => stack.push(State::Braces),
            (_, Some('[')) => stack.push(State::Brackets),
            (_, Some('(')) => stack.push(State::Parentheses),
            (State::Braces, Some('}')) => { stack.pop(); },
            (State::Brackets, Some(']')) => { stack.pop(); },
            (State::Parentheses, Some(')')) => { stack.pop(); },
            (_, Some(unexpected )  )  => {
                if let ')' | ']' | '}' = unexpected {
                    stack.push(State::Missmatch);
                    return stack;
                }
                // do nothing
                ()
            }
            (_, None) => return stack,
        };
    }
    parse(iter, stack)
}

/*

Learning points:

  * lots of pattern matching, I'm sure there are more elegant ways

 */
