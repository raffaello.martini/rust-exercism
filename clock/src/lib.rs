use std::fmt;

#[derive(Debug)]
#[derive(PartialEq)]
pub struct Clock{
    hours: i32,
    minutes: i32,
}

fn overflow(num: i32) -> i32 {
    (num as f32 / 60f32).floor() as i32
}

fn modulus(num: i32, period: i32) -> i32 {
    ((num % period) + period) % period
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
         Clock {
             minutes: modulus(minutes, 60),
             hours: modulus(hours + overflow(minutes), 24),
         }
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        // unimplemented!("Add {} minutes to existing Clock time", minutes);
        Clock::new(0, self.minutes + minutes + self.hours*60)
        }
}

impl fmt::Display for Clock {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:>02}:{:>02}",self.hours,self.minutes)
    }
}

/*
Learning points:

  * Display trait saves time to format structures
  * Debug trait can be derived automatically
  * PartialEq trait can be derived automatically
*/